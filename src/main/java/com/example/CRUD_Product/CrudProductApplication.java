package com.example.CRUD_Product;

import controller.MainRestController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class CrudProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(MainRestController.class, args);
	}
}
