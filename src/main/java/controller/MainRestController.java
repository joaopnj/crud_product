package controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import models.Image;
import models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import repository.ImageRepository;
import repository.ProductRepository;

import javax.validation.Valid;
import java.util.List;

@RestController
@EnableAutoConfiguration
@RequestMapping("/api")
public class MainRestController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    // INDEX

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
    public String index(){
        return "API IS WORKING";
    }

    //ROUTES FOR PRODUCTS


    // SELECTING ALL THE PRODUCTS
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public List<Product> product(){
        return jdbcTemplate.query("SELECT * FROM product", new ProductRepository());
    }

    // SELECTING ALL THE PRODCUTS BY ID
    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public Product getProductById(@PathVariable(value = "id") Long productId) {
        return jdbcTemplate.queryForObject("SELECT * FROM product WHERE id=?", new Object[] {
                        productId
                }, new BeanPropertyRowMapper<Product>(Product.class));
    }

    // INSERTING A PRODUCT
    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public int setProduct(@Valid @RequestBody Product product) {
        return jdbcTemplate.update("INSERT INTO product (id, name, discription) " + "VAlUES(?, ?, ?)",
                product.getId(), product.getName(), product.getDescription());
    }

    // SETTING A FATHER FOR THE PRODUCT
    @RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
    public int setChildProduct(@PathVariable(value = "id") Long productId,
                           @Valid @RequestBody Long fatherId) {
        return jdbcTemplate.update("UPDATE product " + " SET product_id = ? " + " WHERE id = ?",
                fatherId, productId);
    }

    //ROUTES FOR IMAGES


    // SELECTING ALL THE IMAGES
    @RequestMapping(value = "/images", method = RequestMethod.GET)
    public List<Image> images(){
        return jdbcTemplate.query("SELECT * FROM image", new ImageRepository());
    }

    // SETTING A IMAGE WITH THE PRODUCT
    @RequestMapping(value = "/image", method = RequestMethod.POST)
    public int createImage(@Valid @RequestBody Image image) {
        return jdbcTemplate.update("INSERT INTO image (id, type, product_id) " + "VALUES(?,  ?, ?)",
                image.getId(), image.getType(), image.getProduct());
    }

    // SELECTING ALL THE IMAGES BY ID
    @RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
    public Image getImageById(@PathVariable(value = "id") Long imageId) {
        return jdbcTemplate.queryForObject("SELECT * FROM image WHERE id=?", new Object[] {
                imageId
        }, new BeanPropertyRowMapper<Image>(Image.class));
    }

}
