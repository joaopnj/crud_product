var angularModule = angular.module('demo', []);

angularModule.controller('Product', function($scope, $http) {
        $http.get('http://localhost:8080/api/products').
        then(function(response) {
            $scope.products = response.data;
        });
});

angularModule.controller('Image', function($scope, $http) {
        $http.get('http://localhost:8080/api/images').
        then(function(response) {
            $scope.images = response.data;
        });
});

angularModule.controller('CreateProduct', function($scope, $http) {
        $scope.productSubmit = function () {
            var Obj = {
                id : $scope.id,
                name : $scope.name,
                description : $scope.description
            };

            $http.post('http://localhost:8080/api/product', Obj);
        }
});

angularModule.controller('UpdateProduct', function($scope, $http) {
    $scope.updateSubmit = function () {
        var id         = $scope.id;
        var product_id = $scope.product_id;

        $http.post('http://localhost:8080/api/product/'+id, product_id);
    }
});

angularModule.controller('CreateImage', function($scope, $http) {
    $scope.imageSubmit = function () {
        var Obj = {
            name : $scope.name,
            type : $scope.type,
            product_id : $scope.product_id
        };

        $http.post('http://localhost:8080/api/image', Obj);
    }
});