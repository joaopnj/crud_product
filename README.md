## CRUD OF PRODUCTS

This is a CRUD project for Avenue Code Test.

## SPECIFICATIONS

1. The command for test: 'mvn test' is in the pipeline, you can see if the test is successful or not in the main page.
2. The database used it was H2 : **http://www.h2database.com/html/main.html**.
3. The project structure is:
    * Java Spring for BACK-END : **https://spring.io/**
    * Angular for FRONT-END : **https://angularjs.org/.**
4. As required the project use Maven : **https://maven.apache.org/**.

---

## CLONE REPOSITORY

Use this commands bellow to clone the project:

* HTTPS OPTION : **https://(yourusername)@bitbucket.org/(yourusername)/crud_product.git**

* SSH OPTION   : **git@bitbucket.org:(yourusername)/crud_product.git**

* This two commands is available on the top of the page.

---

## BUILD THE PROJECT

To build the project you have to:

1. Clone the project to your computer.
2. Your can import the application and run:
    * With your favorite Java IDE, I suggest: **https://www.jetbrains.com/idea/** 
    * Or executing the command in your terminal: **mvn spring-boot:run**.
3. Once the application is running you should be able to see:
    * The web application on : **http://localhost:8080/**
    * The database on    : **http://localhost:8080/h2**
    * The API            : **http://localhost:8080/api/**
4. The database username and password specifications is in the file : **/src/resources/application.properties'**.


Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, and **Settings** pages.

---